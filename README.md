# ndaal_public_detect_secrets_test_data_reports

## Description

This repository contains the reports of an assessment with different GitLeaks rulesets.  
The reports are placed under [documentation](/documentation) and are available as markdown format.

Furthermore, the ruleset are taken from the following repository:  
<https://gitlab.com/ndaal_open_source/ndaal_secretes_search.git>

