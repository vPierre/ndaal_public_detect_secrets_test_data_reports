# Assessment for Secrets Search using GitLeaks  

Tool: <https://github.com/gitleaks/gitleaks>  
Ruleset: <https://gitlab.com/ndaal_open_source/ndaal_secretes_search/-/raw/master/gitleaks_pii.toml?ref_type=heads>  
Repo assessed: <https://gitlab.com/vPierre/ndaal_public_detect_secrets_test_data>  
Command used: `gitleaks --config=../src/gitleaks_pii.toml -r=../src/gitleaks_pii_2023-09-28.json -f json detect .`  
Files covered: 7/59 (11.86% coverage)  
Total findings: 11/152 (7.24% coverage)  
False Positives: 0  

File Name                                                        | Found/Total | False Positives |
-----------------------------------------------------------------|-------------|-----------------|
dataset/leaky-repo/.bashrc                                       | 2/3         | 0
dataset/leaky-repo/.docker/.dockercfg                            | 2/2         | 0
dataset/leaky-repo/.docker/config.json                           | 2/2         | 0
dataset/leaky-repo/db/dump.sql                                   | 2/10        | 0
dataset/leaky-repo/.bash_profile                                 | 1/6         | 0
dataset/leaky-repo/cloud/heroku.json                             | 1/1         | 0
dataset/leaky-repo/.npmrc                                        | 1/2         | 0
dataset/leaky-repo/.mozilla/firefox/logins.json                  | 0/8         | 0
dataset/leaky-repo/.ssh/id_rsa                                   | 0/1         | 0
dataset/leaky-repo/cloud/.credentials                            | 0/2         | 0
dataset/leaky-repo/cloud/.s3cfg                                  | 0/1         | 0
dataset/leaky-repo/cloud/.tugboat                                | 0/1         | 0
dataset/leaky-repo/db/mongoid.yml                                | 0/1         | 0
dataset/leaky-repo/etc/shadow                                    | 0/1         | 0
dataset/leaky-repo/filezilla/recentservers.xml                   | 0/3         | 0
dataset/leaky-repo/filezilla/filezilla.xml                       | 0/2         | 0
dataset/leaky-repo/misc-keys/cert-key.pem                        | 0/1         | 0
dataset/leaky-repo/misc-keys/putty-example.ppk                   | 0/1         | 0
dataset/leaky-repo/proftpdpasswd                                 | 0/1         | 0
dataset/leaky-repo/web/ruby/config/master.key                    | 0/1         | 0
dataset/leaky-repo/web/ruby/secrets.yml                          | 0/3         | 0
dataset/leaky-repo/web/var/www/.env                              | 0/6         | 0
dataset/leaky-repo/web/var/www/public_html/wp-config.php         | 0/9         | 0
dataset/leaky-repo/web/var/www/public_html/.htpasswd             | 0/1         | 0
dataset/leaky-repo/.git-credentials                              | 0/1         | 0
dataset/leaky-repo/db/robomongo.json                             | 0/3         | 0
dataset/leaky-repo/web/js/salesforce.js                          | 0/1         | 0
dataset/leaky-repo/.netrc                                        | 0/2         | 0
dataset/leaky-repo/hub                                           | 0/1         | 0
dataset/leaky-repo/config                                        | 0/1         | 0
dataset/leaky-repo/db/.pgpass                                    | 0/1         | 0
dataset/leaky-repo/ventrilo_srv.ini                              | 0/2         | 0
dataset/leaky-repo/web/var/www/public_html/config.php            | 0/1         | 0
dataset/leaky-repo/db/dbeaver-data-sources.xml                   | 0/1         | 0
dataset/leaky-repo/.esmtprc                                      | 0/2         | 0
dataset/leaky-repo/web/django/settings.py                        | 0/1         | 0
dataset/leaky-repo/deployment-config.json                        | 0/3         | 0
dataset/leaky-repo/.ftpconfig                                    | 0/3         | 0
dataset/leaky-repo/.remote-sync.json                             | 0/1         | 0
dataset/leaky-repo/.vscode/sftp.json                             | 0/1         | 0
dataset/leaky-repo/sftp-config.json                              | 0/1         | 0
dataset/leaky-repo/.idea/WebServers.xml                          | 0/1         | 0
dataset/git-secret-test/test.txt                                 | 0/2         | 0
dataset/detect-secrets-test-data/files/tmp/file_with_secrets.py  | 0/2         | 0
dataset/detect-secrets-test-data/files/file_with_secrets.py      | 0/1         | 0
dataset/detect-secrets-test-data/files/private_key               | 0/1         | 0
dataset/detect-secrets-test-data/short_files/first_line.php      | 0/1         | 0
dataset/detect-secrets-test-data/short_files/last_line.ini       | 0/3         | 0
dataset/detect-secrets-test-data/short_files/middle_line.yml     | 0/1         | 0
dataset/detect-secrets-test-data/baseline.file                   | 0/29        | 0
dataset/detect-secrets-test-data/config.env                      | 0/1         | 0
dataset/detect-secrets-test-data/config.ini                      | 0/3         | 0
dataset/detect-secrets-test-data/config.md                       | 0/1         | 0
dataset/detect-secrets-test-data/config.yaml                     | 0/3         | 0
dataset/detect-secrets-test-data/config2.yaml                    | 0/2         | 0
dataset/detect-secrets-test-data/each_secret                     | 0/5         | 0
dataset/detect-secrets-test-data/sample.diff                     | 0/1         | 0
