# Assessment for Secrets Search using GitLeaks  

Tool: <https://github.com/gitleaks/gitleaks>  
Ruleset: <https://gitlab.com/ndaal_open_source/ndaal_secretes_search/-/raw/master/gitleaks_all.toml?ref_type=heads>  
Repo assessed: <https://gitlab.com/vPierre/ndaal_public_detect_secrets_test_data>  
Command used: `gitleaks --config=../src/gitleaks_all.toml -r=../src/gitleaks.json -f json detect .`  
Files covered: 33/59 (55.93% coverage)  
Total findings: 94/152 (61.84% coverage)  
False Positives: 14  

File Name                                                        | Found/Total | False Positives |
-----------------------------------------------------------------|-------------|-----------------|
dataset/detect-secrets-test-data/baseline.file                   | 26/29       | 0
dataset/leaky-repo/db/dump.sql                                   | 12/10       | 2
dataset/leaky-repo/web/var/www/public_html/wp-config.php         | 7/9         | 0
dataset/leaky-repo/web/var/www/.env                              | 6/6         | 0
dataset/leaky-repo/.bash_profile                                 | 5/6         | 0
dataset/leaky-repo/.bashrc                                       | 5/3         | 2
dataset/detect-secrets-test-data/config.ini                      | 5/3         | 2
dataset/leaky-repo/.docker/.dockercfg                            | 4/2         | 2
dataset/leaky-repo/.docker/config.json                           | 4/2         | 2
dataset/leaky-repo/cloud/.credentials                            | 4/2         | 2
dataset/leaky-repo/web/ruby/secrets.yml                          | 3/3         | 0
dataset/leaky-repo/cloud/heroku.json                             | 2/1         | 1
dataset/leaky-repo/filezilla/recentservers.xml                   | 2/3         | 0
dataset/git-secret-test/test.txt                                 | 2/2         | 0
dataset/detect-secrets-test-data/config.md                       | 2/1         | 1
dataset/detect-secrets-test-data/config.yaml                     | 2/3         | 0
dataset/leaky-repo/.ssh/id_rsa                                   | 1/1         | 0
dataset/leaky-repo/cloud/.s3cfg                                  | 1/1         | 0
dataset/leaky-repo/etc/shadow                                    | 1/1         | 0
dataset/leaky-repo/filezilla/filezilla.xml                       | 1/2         | 0
dataset/leaky-repo/misc-keys/cert-key.pem                        | 1/1         | 0
dataset/leaky-repo/misc-keys/putty-example.ppk                   | 1/1         | 0
dataset/leaky-repo/proftpdpasswd                                 | 1/1         | 0
dataset/leaky-repo/.npmrc                                        | 1/2         | 0
dataset/leaky-repo/web/var/www/public_html/.htpasswd             | 1/1         | 0
dataset/leaky-repo/.git-credentials                              | 1/1         | 0
dataset/leaky-repo/web/js/salesforce.js                          | 1/1         | 0
dataset/leaky-repo/db/.pgpass                                    | 1/1         | 0
dataset/leaky-repo/web/var/www/public_html/config.php            | 1/1         | 0
dataset/leaky-repo/web/django/settings.py                        | 1/1         | 0
dataset/detect-secrets-test-data/files/private_key               | 1/1         | 0
dataset/detect-secrets-test-data/config.env                      | 1/1         | 0
dataset/detect-secrets-test-data/sample.diff                     | 1/1         | 0
dataset/leaky-repo/.mozilla/firefox/logins.json                  | 0/8         | 0
dataset/leaky-repo/cloud/.tugboat                                | 0/1         | 0
dataset/leaky-repo/db/mongoid.yml                                | 0/1         | 0
dataset/leaky-repo/web/ruby/config/master.key                    | 0/1         | 0
dataset/leaky-repo/db/robomongo.json                             | 0/3         | 0
dataset/leaky-repo/.netrc                                        | 0/2         | 0
dataset/leaky-repo/hub                                           | 0/1         | 0
dataset/leaky-repo/config                                        | 0/1         | 0
dataset/leaky-repo/ventrilo_srv.ini                              | 0/2         | 0
dataset/leaky-repo/db/dbeaver-data-sources.xml                   | 0/1         | 0
dataset/leaky-repo/.esmtprc                                      | 0/2         | 0
dataset/leaky-repo/deployment-config.json                        | 0/3         | 0
dataset/leaky-repo/.ftpconfig                                    | 0/3         | 0
dataset/leaky-repo/.remote-sync.json                             | 0/1         | 0
dataset/leaky-repo/.vscode/sftp.json                             | 0/1         | 0
dataset/leaky-repo/sftp-config.json                              | 0/1         | 0
dataset/leaky-repo/.idea/WebServers.xml                          | 0/1         | 0
dataset/detect-secrets-test-data/files/tmp/file_with_secrets.py  | 0/2         | 0
dataset/detect-secrets-test-data/files/file_with_secrets.py      | 0/1         | 0
dataset/detect-secrets-test-data/short_files/first_line.php      | 0/1         | 0
dataset/detect-secrets-test-data/short_files/last_line.ini       | 0/3         | 0
dataset/detect-secrets-test-data/short_files/middle_line.yml     | 0/1         | 0
dataset/detect-secrets-test-data/config2.yaml                    | 0/2         | 0
dataset/detect-secrets-test-data/each_secret                     | 0/5         | 0
